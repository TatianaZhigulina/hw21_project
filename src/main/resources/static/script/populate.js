fetch("/civilian").then(response => response.json()).then(
data => {
  Object.keys(data).forEach((key) => {
    let item = document.createElement("li");
    let link = document.createElement("a");
    link.setAttribute("target", "_blank");
    item.id = key;
    link.innerHTML = `${data[key].firstName} ${data[key].secondName} ${data[key].occupation.toLowerCase()}. Age: ${data[key].age}`;
    link.href = `/person/${key}`;
    item.appendChild(link);
    document.getElementById("listContainer").appendChild(item);
  })
});

document.cookie="user=Weyland-Yutani;  path=/";
document.cookie="catchPhrase=Crew expendable;";
document.cookie="authCode="+Math.floor(Math.random()*1000000)+";";